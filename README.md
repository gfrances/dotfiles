

Generic Dotfiles
======================

A collection of configuration files to setup various systems, inspired by https://github.com/johnjohndoe/dotfiles.

Installation
------------

First, grand execute permissions to the included shell script.
Then run the script to install the configuration files.

    chmod +x INSTALL.sh && ./INSTALL.sh


The install script creates a soft link for each configuration file using 
e.g. the following command:

    ln -sv ~/dotfiles/bashrc ~/.bashrc


Some configuration files require additional steps:

* The `.gdbinit` configuration file requires some Python libstdc++ printers to be installed, as described in <https://sourceware.org/gdb/wiki/STLSupport>.
