# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# Source global definitions - This is currently just used in the unibas cluster
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


################################################################################
# CUSTOMIZATIONS
################################################################################

# Nicer prompt
PS1="\[\033[35m\]\t\[\033[m\]-\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "

# Disable XON/XOFF flow control, as per https://stackoverflow.com/a/791800
stty -ixon

# Add the local bin directory to the PATH
export PATH=${HOME}/bin:$PATH

# Set locale to some locale common in cluster and local machines
export LANG=en_GB.utf8

# Load some useful functions, if available
test -r ~/.functions && . ~/.functions

# Local C++ library installations
if [[ -d /usr/local/lib ]]; then
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
fi
if [[ -d ${HOME}/local/lib ]]; then
	export LIBRARY_PATH=$LIBRARY_PATH:${HOME}/local/lib
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${HOME}/local/lib
fi

if [[ -d ${HOME}/local/include ]]; then
	export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:${HOME}/local/include
fi

# Boost local installation
if [[ -d ${HOME}/local/include/boost ]]; then
	export BOOST_ROOT=${HOME}/local
fi

# Planning-related paths: LAPKT
export LAPKT="${HOME}/projects/code/lapkt-installation"
#export LAPKT2_PATH="${HOME}/projects/code/lapkt/lapkt"
#export LAPKT_NOVELTY_PATH="${HOME}/projects/code/lapkt-novelty"
export PLANNING_EXP="${HOME}/projects/code/planning-vm-experiments"
export FS_PATH="${HOME}/projects/code/fs"
export FSBENCHMARKS="${HOME}/projects/code/fs-benchmarks"

# Clingo C++ Library
export GRINGO_PATH="${HOME}/lib/clingo"
#if [[ -d ${CLINGO_PATH}/build/release ]]; then
#	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${CLINGO_PATH}/build/release
#fi

# CLINGO Python Module
#if [[ -d ${CLINGO_PATH}/build/release/python ]]; then
#    export PYTHONPATH="${CLINGO_PATH}/build/release/python:$PYTHONPATH"
#fi

# LAPKT and FS libraries
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${FS_PATH}/lib
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${LAPKT}/lib

# If the module script is available, use to load it a recent version of GCC
if hash module 2>/dev/null; then
    module purge
    module load Boost/1.65.1-foss-2017a-Python-3.6.4  # Will load GCCcore/6.3.0 as well
    module load SCons/3.0.1-foss-2017a-Python-3.6.4  # Will load Python/3.6.4-foss-2017a as well
    module load OpenSSL/1.1.0e-foss-2017a  # Necessary git+ssh to work

    # Alt:
    # if  !(module spider Python/2.7.11-goolf-1.7.20) |& grep error; then module load Python/2.7.11-goolf-1.7.20; fi
    #module try-load matplotlib/1.5.1-goolf-1.7.20-Python-2.7.11
    #module try-load CMake/2.8.12-goolf-1.7.20
    #elif module spider GCC/5.4.0-2.26 2>/dev/null; then
    #    module load GCC/5.4.0-2.26
fi


# ANTLR
export ANTLR_PATH="$HOME/lib/antlr-4.7.1-complete.jar"
export CLASSPATH=".:$ANTLR_PATH:$CLASSPATH"
alias antlr4='java -jar $ANTLR_PATH'
alias grun='java org.antlr.v4.gui.TestRig'

# GOOGLE TEST
export GTEST_DIR="${HOME}/projects/code/googletest/googletest"

# Microsoft's Z3 SMT solver
# export Z3_PATH="${HOME}/local/z3-z3-4.5.0/"
# if [[ -d "${Z3_PATH}/build/python" ]]; then
#     export PYTHONPATH="${Z3_PATH}/build/python:$PYTHONPATH"
# fi

# Validate
export PATH=${HOME}/projects/code/VAL:$PATH

# Arcade Learning Environment
# export ALE_PATH="${HOME}/projects/code/Arcade-Learning-Environment-v0.5.1"
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${ALE_PATH}

export DOWNWARD_BENCHMARKS="${HOME}/projects/code/downward-benchmarks"
export HTG_BENCHMARKS="${HOME}/projects/code/htg-domains"

export DOWNWARD_CPLEX_ROOT64="${HOME}/lib/CPLEX_Studio1271/cplex"
export DOWNWARD_COIN_ROOT64="${HOME}/lib/coin64"

export PYBIND=~/projects/code/pybind11
export DOWNWARD_PYBIND=${PYBIND}
export DOWNWARD_LATEST="${HOME}/projects/code/downward/downward-latest"

# Virtualenvwrapper
if [ -e /usr/local/bin/virtualenvwrapper.sh ]; then
    export WORKON_HOME=~/lib/virtualenvs
    VIRTUALENVWRAPPER_PYTHON='/usr/bin/python3' # This needs to be placed before the virtualenvwrapper command
    source /usr/local/bin/virtualenvwrapper.sh
fi

# ROS
source /opt/ros/noetic/setup.bash

# Go language:
export GOPATH=~/go
export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin

# Rust language:
. "$HOME/.cargo/env"
