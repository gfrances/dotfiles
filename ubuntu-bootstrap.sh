#!/bin/bash

set -euo pipefail

# Following package list works for Ubuntu 18.04 LTS

sudo apt install -y \
vlc dos2unix terminator vim tree chromium-browser indicator-multiload \
build-essential g++ clang make cmake scons flex bison libboost-all-dev pkgconf \
python3-dev python3-pip python3-virtualenv python3-setuptools python3-dbg \
git mercurial gitk meld \
clang-format clang-tidy cgdb valgrind kcachegrind massif-visualizer \
texlive texlive-fonts-extra texlive-science texlive-latex-extra texlive-science texlive-bibtex-extra texlive-publishers \
openssh-server apt-transport-https ca-certificates curl software-properties-common compizconfig-settings-manager squashfs-tools \
fonts-inconsolata

sudo pip3 install virtualenvwrapper

# For the installation of fonts such as fonts-inconsolata
sudo fc-cache -fv

# A few settings
gsettings set org.gnome.shell.app-switcher current-workspace-only true

