#!/bin/bash

BASEDIR=$(pwd)
echo "Linking dotfiles from $BASEDIR to home directory."
echo

for file in {bashrc,functions,gdbinit,gitattributes,gitconfig,gitignore,hgignore,hgrc,sshconfig,vimrc}; do
    ln -sv "$BASEDIR"/"$file" ~/."$file"
done
unset file

mkdir -p ~/.ssh/
ln -sv "$BASEDIR"/"ssh_config" ~/.ssh/config
ln -sv "$BASEDIR"/"ai-repos.key" ~/.ssh/ai-repos.key
